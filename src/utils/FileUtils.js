import axios from 'axios'

export default {
	getPublicFile: (filename, callback) => {
		axios.get(process.env.PUBLIC_URL + "/" + filename)
		.then((response) => {
			callback(response.data)
		})
	}
}