import React, {Component} from 'react'

import '../../App.css'

import SlideShow from '../../components/SlideShow'
import FileUtils from '../../utils/FileUtils'
import About from './About'

import { FacebookProvider, Page } from 'react-facebook';

export default class Home extends Component {
	state = {
		facebook: null
	}

	componentDidMount() {
		let self = this
		FileUtils.getPublicFile("SiteData_en.json", (data) => {
			self.setState({facebook: data.facebook})
		})
	}

	render() {
		return (
			<div className="home-container">
				<div className="slide-show">
					<SlideShow time={5000} />
				</div>
				<div className="home-content">
					<div className="about">
						<About />
					</div>
					<div className="event-modal">
						{this.state.facebook ?
						<FacebookProvider appId={this.state.facebook.apiKey}>
							<Page href={"https://www.facebook.com/" + this.state.facebook.groupName} tabs="events, timeline" />
						</FacebookProvider> : null
						}
					</div>
				</div>
			</div>
		)
	}
}