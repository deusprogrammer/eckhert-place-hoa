import React, { Component } from 'react';
import '../App.css';

import {NavLink, Route, Switch} from 'react-router-dom'

import Home from './routes/Home'
import Board from './routes/Board'

import Burger from "../components/Burger"

import FileUtils from '../utils/FileUtils'

class AppEn extends Component {
  state = {
    isSpanish: false,
    currentRoute: "/",
    links: {}
  }

  onLangSwitch = (isSpanish) => {
    let path = window.location.pathname.split("/", 3)[2]
    this.props.history.push("/es/" + path)
  }

  componentDidMount() {
    let self = this
    FileUtils.getPublicFile("SiteData_en.json", (data) => {
      self.setState({links: data.links})
      document.title = data.title
    })
  }

  onNavChange = (route) => {
    this.setState({currentRoute: route})
  }

  render() {
    return (
      <div className="App">
        <header>
          <div className="banner">
            <div className="logo">
              <img src={process.env.PUBLIC_URL + "/images/logo.png"} alt="Logo" />
            </div>
          </div>
          <Burger>
            <div className="nav">
              <nav>
                <NavLink to={`${this.props.match.path}/`} exact activeClassName="selected" onClick={() => {this.onNavChange("")}}>Home</NavLink>
                {this.state.links.documents ? <a href={this.state.links.documents} target="_blank">Documents</a> : null}
                <NavLink to={`${this.props.match.path}/board`} activeClassName="selected" onClick={() => {this.onNavChange("/board")}}>Board</NavLink>
                {this.state.links.login ? <a href={this.state.links.login}>Login</a> : null}
              </nav>
              <div className="switch-parent">
                <div className="switch-container">
                  <div>
                    English
                  </div>
                  <div>
                    <label className="switch">
                      <input type="checkbox" value={this.state.isSpanish} onChange={() => {this.onLangSwitch(!this.state.isSpanish)}} checked={this.state.isSpanish}/>
                      <span className="slider round"></span>
                    </label>
                  </div>
                  <div>
                    Espanol
                  </div>
                </div>
              </div>
            </div>
          </Burger>
        </header>

        <div className="content">
          <Switch>
            <Route path={`${this.props.match.path}/`} exact component={Home} />
            <Route path={`${this.props.match.path}/board`} component={Board} />
          </Switch>
        </div>

        <footer>
        </footer>
      </div>
    );
  }
}

export default AppEn;