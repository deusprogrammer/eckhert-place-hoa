import React, {Component} from 'react'

import '../../App.css'

import FileUtils from '../../utils/FileUtils'

export default class Board extends Component {
	state = {
		sections: [],
		headerName: ""
	}

	componentDidMount() {
		let self = this
		FileUtils.getPublicFile("SiteData_es.json", (data) => {
			self.setState({sections: data.boardMembers})
		})
	}

	render() {
		return (
			<div>
				<h3>Mesa Directiva</h3>

				{this.state.sections.map((element, index) => {
					return (
						<div className="board-element">
							<div className="board-element-image">
								<img src={"/" + process.env.PUBLIC_URL + element.image} alt={element.fullName + " image"} />
								<div className="board-element-name">{element.fullName}</div>
								<div className="board-element-position">{element.position}</div>
								<div className="board-element-contact">
									<div>Extensión: {element.extension}</div>
									<div>Email: {element.email}</div>
								</div>
							</div>
							<div className="board-element-data">
								<div className="board-element-bio">
									{element.bio.map((paragraph) => {
										return (
											<p>{paragraph.text}</p>
										)
									})}
								</div>
							</div>
						</div>
					)
				})}
			</div>
		)
	}
}