import React, {Component} from 'react'

import '../../App.css'

import FileUtils from '../../utils/FileUtils'

import ReactMarkdown from 'react-markdown'

export default class About extends Component {
	state = {
		about: ""
	}

	componentDidMount() {
		let self = this
		FileUtils.getPublicFile("markdown/about_es.md", (data) => {
			console.log(data)
			self.setState({about: data})
		})
	}

	render() {
		return (
			<div>
				<ReactMarkdown source={this.state.about} />
			</div>
		)
	}
}