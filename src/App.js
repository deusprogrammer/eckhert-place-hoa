import React, { Component } from 'react';
import './App.css';

import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom'

import AppEn from './en/AppEn'
import AppEs from './es/AppEs'

class App extends Component {

  render() {
    return (
      <Router>
        <Switch>
          <Route path="/en" component={AppEn} />
          <Route path="/es" component={AppEs} />
          <Redirect from="/" to="en" />
        </Switch>
      </Router>
    );
  }
}

export default App;
