import React, {Component} from 'react'

import ImageGallery from 'react-image-gallery'

import "react-image-gallery/styles/css/image-gallery.css"
import "../App.css"

import FileUtils from "../utils/FileUtils"

export default class SlideShow extends Component {
	state = {
		images: []
	}

	componentDidMount() {
		FileUtils.getPublicFile("SiteData_en.json", (data) => {
			let images = []
			for (var i in data.slideShow) {
				let element = data.slideShow[i]
				images.push({
					original: "/" + process.env.PUBLIC_URL + element.image,
					originalAlt: element.imageAlt
				})
			}
			this.setState({images: images})
		})
	}

	changeImage() {
		let imageIndex = this.state.imageIndex < this.state.images.length - 1 ? this.state.imageIndex + 1 : 0
		this.setState({imageIndex: imageIndex})
	}

	render() {
		return (
			<ImageGallery
				items={this.state.images}
				autoPlay={true}
				showBullets={true}
				showThumbnails={false}
				showPlayButton={false}
				showFullscreenButton={false}
				showNav={false}
				useBrowserFullscreen={false}
				slideDuration={3000}
				/>
		)
	}
}