import React from 'react'

import classNames from 'classnames'

export default class Burger extends React.Component {
	state = {
		burgerOpen: false
	}

	toggleBurger = () => {
	    this.setState({burgerOpen: !this.state.burgerOpen})
  	}

	render() {
		return (
			<div className="burger">
	            <div className={classNames({
	              "burger-icon": true,
	              "change": this.state.burgerOpen
	            })} onClick={() => {this.toggleBurger()}}>
					<div className="bar1"></div>
					<div className="bar2"></div>
					<div className="bar3"></div>
	            </div>
	            <div className={classNames({
	            	"burger-children": true,
	            	"hide-burger-children": !this.state.burgerOpen
	            })} onClick={() => {this.toggleBurger()}}>
	            	{this.props.children}
	            </div>
			</div>
          )
	}
}